---
title: "Peut-on conserver la nature comme on conserve les sardines ? Episode 2"
date: 2019-11-22T10:10:00+02:00
publishdate: 2019-11-22T10:10:00+02:00
image: "/img/blog/biologie_conservation/thumbnail.jpg"
tags: ["Environnement"]
comments: false
---

`Écrit par Sophie Evers pour Bicursiosité`

*N'hésitez pas à consulter le cadre vocabulaire en bas de l'article.*

# EPISODE II: D’où viennent les valeurs en biologie de la conservation? 

Dans l’article précédent, j’ai introduit la question à laquelle je tente de répondre: quelle est la place des valeurs en biologie de la conservation ? Pour répondre à cette vaste question, trois épisodes se succèdent. Tout d’abord, j’ai présenté les différentes étapes de la biologie de la conservation qui introduisent des valeurs non-épistémiques dans la discipline (voir l’Episode I). Puis je me demande pourquoi la biologie de la conservation est intrinsèquement liée à ces choix, donc aux valeurs non-épistémiques. Enfin, j’examinerai une étude de cas pour justifier que la biologie de la conservation doit intégrer les valeurs à sa pratique, à sa méthodologie et à l’analyse de ses données. 
Certaines sciences, telles que la chimie, la physique, ou d’autres sous-disciplines de la biologie, semblent moins étroitement liées aux valeurs non-épistémiques que ne l’est la biologie de la conservation. Pourquoi la biologie de la conservation est-elle tant imprégnée de valeurs ? 

## 1) Conserver les sardines pour mieux les manger
Quand on se penche sur l’origine de la discipline et les causes de son développement, on aperçoit le lien étroit entre biologie de la conservation et société. Notamment, la création de la biologie de la conservation comme discipline scientifique remonte à la moitié du XXème siècle, lorsqu’on s’est rendu compte que les ressources naturelles étaient consommées plus rapidement qu’elles n’étaient régénérées, et que la population humaine se trouverait en danger quand ces ressources viendraient à manquer. L’un de ses fondateurs, Soulé (1985), décrit cette nouvelle science comme étant une discipline de crise [^1]. L’idée de conserver la nature est issue d’une idée de nécessité de préserver les ressources pour la survie de la population humaine. Le lien à la société est donc originellement intrinsèque à la biologie de la conservation. C’est dans un rapport utilitaire à la nature que la notion de conservation a émergé, et c’est donc dans la première optique de pouvoir continuer à exploiter les ressources de la nature que la conservation s’est développée. Redford et al. (1999) écrivent: « We live in a world of use » pour décrire la dimension utilitariste de la conservation. Ils établissent même une distinction intéressante entre la préservation, qui serait le fait de ne pas toucher à la nature, et la conservation qui viserait à une utilisation de la nature sans la détruire ou la convertir entièrement. Le concept de ‘service écosystémique’, c’est-à-dire d’un service offert par un écosystème pour les humains, témoigne lui aussi de la valeur instrumentale de la conservation de la nature puisqu’il s’agit d’estimer la valeur d’un écosystème en fonction des bénéfices qu’il apporte à la population humaine. Par exemple, la mer offre de nombreux services tels que l’approvisionnement en nourriture (poissons, crustacés), la régulation du climat (par l’absorption du carbone), ou un espace de récréation (activités nautiques, sorties à la plage), etc. 

![Bateau de pêche en Irlande](/img/blog/biologie_conservation/DSC_1902.JPG)  
*Petit bateau de pêche le long de la côte irlandaise (Howth), photo prise par Sophie Evers* 

## 2) La valeur intrinsèque de la nature

### La nature vaut pour elle-même
La volonté de préserver la nature n’est-elle que liée aux valeurs des conséquences sur l’humanité, ou est-il possible de concevoir une valeur intrinsèque à la nature ? Historiquement, les valeurs esthétiques, sacrées ou religieuses qui donnent à la nature une valeur en soi, ont toujours existé à travers les cultures. Dans ces cas-là, la nature est conservée pour elle-même et non pour les humains. En Afrique Centrale, par exemple, et notamment au Cameroun, certaines zones boisées sont préservées parce qu’elles contiennent des esprits ou des divinités. Les étrangers, tels que les touristes, ne peuvent y entrer qu’ accompagnés par un guide initié du village. De cette manière, des forêts dites ‘sacrées’ sont conservées dans chaque zone de villages pour leur valeur intrinsèque que leur confère les populations locales. Le concept de ‘nature’ étant ancré dans les cultures humaines à travers la religion, l’art ou la tradition, il n’est pas étonnant qu’un respect, de l’ordre même du devoir moral, se soit lui aussi ancré dans la pensée humaine à son égard. Un autre exemple peut être le babouin qui, parce qu’il est un être vivant, a une valeur en soi portée par la vie indépendamment de sa relation avec les humains.

![Babouins chacma](/img/blog/biologie_conservation/DSC_2707.JPG)  
*Babouins chacma (Papio ursinus) dans la Province du Limpopo (Afrique du Sud), photo prise par Sophie Evers* 

### Biodiversité et valeur intrinsèque
Parler de valeur intrinsèque de la biodiversité semble plus problématique puisque nous avons présenté la biodiversité comme un concept-outil issu de la biologie de la conservation. Mais nous avons aussi vu que pour justifier qu’il faille conserver la biodiversité, il faut admettre que l’on donne une valeur à la biodiversité. L’article The Value of Phylogenetic Diversity (Pellens et al. 2016) aborde la question de valeur intrinsèque de la biodiversité, pour laquelle deux interprétations possibles sont proposées. 
La première consiste à dire que la biodiversité a une valeur intrinsèque au sens où elle a une valeur supérieure ou supplémentaire à sa valeur instrumentale. Si la valeur instrumentale est définie comme une utilisation de la biodiversité pour une fin strictement scientifique ou économique, nous pourrions dire par exemple qu’il existe d’autres fins récréatives ou esthétiques liées à la richesse naturelle qui sont donc intrinsèques à la nature.
La seconde interprétation de la valeur intrinsèque de la biodiversité est celle de dire qu’il y a une valeur en dehors de l’évaluation qui est faite et des humains qui évaluent. Ainsi, la disparition de toute personne qui donne une valeur à une entité (qui évalue la biodiversité) n’empêche pas que cette entité (la biodiversité) continue à avoir une valeur en soi.

## 3) Intégrer ou écarter les valeurs

### Value-free science
Alors, que faut-il faire de ces valeurs? Ce qui pose problème lorsque l’on introduit des valeurs non-épistémiques au sein d’une discipline scientifique, c’est l’introduction d’un discours non objectif, voire non rationnel. Après le constat qui a été fait d’une imprégnation par les choix et les valeurs à tous les niveaux de la biologie de la conservation, nous pourrions être tentés de dire qu’il ne s’agit pas d’une science au même titre que les autres. Douglas (2007) analyse la question d’absence de valeurs en sciences (ou ‘value-free science’ en anglais) dans le cadre d’une science qui serait appliquée en politique publique. Son premier constat, c’est que les valeurs non-épistémiques sont essentielles au sein de la science dans les choix de méthodes d’observation, d’analyses, et d’estimation de l’erreur. Parce que les valeurs sont présentes dans les fondements des différents aspects de la discipline, on ne peut pas les éliminer sans détruire la discipline. Le deuxième constat, c’est qu’il n’est pas souhaitable d’enlever au chercheur scientifique la responsabilité de penser aux choix et conséquences de ses travaux de recherche en termes de valeurs. Comment faire sinon pour comprendre le choix de telle ou telle méthode, comment interpréter ses résultats de manière critique, comment proposer des projets qui sont en accord avec la réalité? Pour finir, Douglas présente différentes définitions de l’objectivité qui sauvent la science qui n’est pas ‘value-free’. Il existe d’autres critères d’objectivité qui montrent qu’une science imprégnée de valeurs aspire quand même à la connaissance, telle que l’objectivité convergente quand plusieurs études ayant des méthodes différentes aboutissent à une même conclusion. 

### ‘Value-full’ conservation biology
Cette analyse de la place des valeurs en sciences semble très pertinente pour étudier le cas de la biologie de la conservation, qui est une science ayant des applications directes dans la société, donc des conséquences directes aussi. Comme Douglas, je défends que les biologistes de la conservation ne devraient pas et ne peuvent pas déléguer les questions des valeurs à d’autres spécialistes externes, mais doivent eux-mêmes y répondre consciemment. Pellens et al. (2016) concluent que la grande incertitude, tel un labyrinthe, qui se trouve au niveau des méthodes de la conservation biologique a pour origine l’incertitude sur les raisons d’agir pour la conservation de la nature. Mon hypothèse est de dire que cette incertitude observée dans le domaine scientifique est issue de l’absence de prise en compte des valeurs non-épistémiques qui entrent en jeu dans la biologie de la conservation. Pour sortir du labyrinthe, il semblerait donc que la biologie de la conservation doive accepter et travailler sur ses valeurs… un bon point pour les philosophes de la biologie, les historiens de la biologie, les anthropologues, les sociologues de la biologie, avec lesquels la biologie de la conservation devrait travailler main dans la main.

# Conclusion
La biologie de la conservation est une science qui entre directement en contact avec la société et avec la politique publique. On conserve parce qu’il y a une finalité, on conserve pour quelque chose, et dans notre cas, on conserve la nature pour l’humain. Pour l’humain c’est parce qu’on a besoin de la nature pour survivre, c’est une ressource finie dans laquelle on puise sans arrêt. La conserver nous permettrait de continuer plus longtemps à l’utiliser. Pour l’humain c’est aussi parce que nous avons un contact tout particulier avec la nature, elle fait partie de notre histoire, de notre imaginaire, de nos mythes, de nos religions, de notre inspiration…On conserve la nature pour ses qualités intrinsèques qui la lie à l’humain. Enfin, on conserve aussi la nature pour la nature, pour sa valeur intrinsèque en présence ou non de l’humain. 
Face à l’importance des valeurs dans la raison d’être de la biologie de la conservation, j’ai proposé l’hypothèse selon laquelle les biologistes devraient être conscients et responsables de leurs choix non-épistémiques. Une petite étude de cas nous aidera à l’illustrer..dans le dernier épisode!

#### Vocabulaire:

> **Service écosystémique**: un bénéfice pour l’humain tiré d’une fonction écologique d’un écosystème. Par exemple, un écosystème peut fournir un approvisionnement en ressources, une régulation de phénomènes météorologiques, un intérêt culturel ou ludique, etc.   
> **Valeur instrumentale** : c'est la valeur attribuée à une entité (ici la nature ou la biodiversité) en fonction des bénéfices qu’elle apporte à d’autres entités (ici les humains).
> **Valeur intrinsèque** : c’est la valeur en soi d’une entité (ici la nature), indépendamment de son utilité pour d’autres entités (ici les humains).

#### Bibliographie

1. Douglas, Heather. "Rejecting the ideal of value-free science." (2007).
2. Pellens, Roseli, and Philippe Grandcolas. Biodiversity Conservation and Phylogenetic Systematics. Springer, 2016.
3. Redford, Kent H., and Brian D. Richter. "Conservation of biodiversity in a world of use." Conservation biology 13.6 (1999): 1246-1256.
4. Soulé, Michael E. "What is conservation biology?." BioScience 35.11 (1985): 727-734.

#### Note

[^1]: Soulé (1985): “Conservation biology differs from most other biological sciences in one important way: it is often a crisis discipline. Its relation to biology, particularly ecology, is analogous to that of surgery to physiology and war to political sciences.”
