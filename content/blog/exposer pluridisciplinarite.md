---
title: "Pour exposer la pluridisciplinarité à l’Humanité: Bicursiosité lance son blog"
date: 2019-03-06T19:46:22+02:00
publishdate: 2019-03-06T19:46:22+02:00
image: "/img/blog/p.jpg"
tags: []
comments: false
---
`Écrit par Thomas Laville pour Bicursiosité`

*« Il nous faut dissiper l’illusion qui prétend que nous serions arrivés à la société de la connaissance. En fait, nous sommes parvenus à la société des connaissances séparées les unes des autres, séparation qui nous empêche de les relier pour concevoir les problèmes fondamentaux et globaux tant de nos vies personnelles que de nos destins collectifs. » Edgar Morin, La Voie, Fayard, 2011*

En 2017, l’association *Bicursiosité* a été créée avec pour ambition de réunir tous les double-cursus et majeurs-mineurs des facultés des Lettres et des Sciences de Sorbonne Université (sciences & philosophie/histoire/musicologie/allemand/chinois/linguistique ainsi que lettres et informatique). Dans cette optique, *Bicursiosité* s’est dotée de son propre site internet en octobre 2018. Ce dernier a plusieurs finalités : renseigner sur les double-cursus, aider les étudiants et étudiantes actuellement en double-cursus et communiquer sur les évènements de l’association. Dorénavant, le site se dotera d’une nouvelle section : le blog. 

## Mais pourquoi un Blog ? 

Pour comprendre son intérêt, il faut revenir aux objectifs définis lors de la création de l’association.  Deux des objectifs sont les suivants :

* améliorer la visibilité des double-cursus (DC) en Sciences et Humanités au sein des milieux scolaire et universitaire ;
* apporter aux étudiants et étudiantes de double-cursus (mais pas seulement) une offre culturelle, un réseau étudiant et une portée vers l’orientation professionnelle.

Jusqu’à présent, les membres de *Bicursiosité* se sont efforcé.e.s de répondre à ces objectifs en organisant des évènements, soit à destination des étudiants et étudiantes actuel.le.s en DC (Speed-meeting), soit ouverts à tous (Causeries musicales). Cependant, nous souhaitons aller plus loin et garder une réel témoin de l’intérêt et de la nécessité de la pluridisciplinarité[^1] et de l’interdisciplinarité[^2]


Les approches interdisciplinaires se développent de plus en plus dans le milieu de la recherche et la création de **l’Institut de la transition environnementale Sorbonne Université** en est une preuve[^3]. De notre côté, les Causeries musicales autour des thèmes de l’être et du faire dans l’art et les sciences mais aussi de l’identité et des nouvelles technologies ont suscité un réel échange, révélateur de  l’importance de parler de tels sujets[^4]. Cela s’est notamment démontré par l’intérêt de personnes externes aux double-cursus et donc moins habituées aux dialogues entre disciplines.

A partir de ce constat, nous en sommes venus à la conclusion qu’il fallait donner la possibilité à nos membres de mettre à l’écrit, de publier toutes ces idées et d’explorer plus largement l’intérêt de la pluridisciplinarité et de l’interdisciplinarité. C’est ainsi que le blog a fait son apparition. Il permettra d’initier un public plus large et diversifié, enrichissant ainsi la conversation sur l’interdisciplinarité et la pluridisciplinarité et sur la nécessité d’ouvrir ses horizons.

## Pourquoi écrire un article ? 

La raison principale est bien évidemment de partager au plus grand nombre des thèmes qui vous importent, qui permettent de montrer l’intérêt de la pluridisciplinarité et de ses approches. Les écrits favorisent l’accès et le partage des idées et des connaissances, offrant une base concrète et structurée pour engager la discussion.  
   
La volonté de développer notre blog vient aussi de notre envie d’offrir aux étudiants et étudiantes de DC la possibilité de développer leurs capacités professionnelles, notamment de synthèse et de rédaction. Chaque année des étudiants et étudiantes sont intéressé.e.s par le journalisme et la communication scientifique et se demandent s’ils sont faits pour cela  et s’ils devraient adapter leur orientation académique en conséquence. Ecrire un article sur le blog est un bon moyen de se faire la main et de garder une trace de son travail sur un support officiel. Cette trace sera un atout certain pour le futur professionnel de nos membres, pouvant être intégrée à leur portfolio personnel. L’expérience de rédaction pourra aussi être intégrée au CV et au profil LinkedIn de nos membres (puisque notre association a désormais son compte LinkedIn). 

## Qu’écrire et comment l’écrire ? 

*Bicursiosité* considère que l’essentiel, c’est de parler de ce qu’on l’on aime, de ce qui nous tient à coeur, notamment quand c’est en lien avec l’interdisciplinarité et la pluridiscplinarité. Le sujet de l’article peut tourner autour de la vulgarisation d’un article scientifique, le décorticage d’une actualité en lien avec les sciences, le partage de son expérience pluridisciplinaire (projet de recherche, stage, conférence, exposition, congrès, etc.) ou plus simplement présenter un sujet qui nous passionne.  
Le but n’est pas d’écrire un manifeste en faveur de ses idées scientifiques ou politiques. Cependant, il est bien évidemment possible de parler de mesures politiques impactant le monde de la recherche par exemple. Le tout est de rester impartial et mesuré tout en argumentant et en présentant des exemples.  
Pour ce qui est de ‘comment publier l’article ?’, c’est assez simple ! Toutes les étapes ont été consignées dans le mode d’emploi que l’on peut  retrouver en cliquant [ici](http://bicursiosite.com/files/modeemploi.pdf).

## Comment participer ?  

Tu as déjà des idées ? Un premier jet ? Tu peux contacter l’association directement sur notre [Facebook](https://www.facebook.com/bicursiosite/) ou par mail ([bicursiosite@ntymail.com](mailto:bicursiosite@ntymail.com)). Si tu as peur d’écrire au concept qu’est une association, tu trouveras un contact plus humain en écrivant à Minh Thi et Sophie par mail ([mint.mercier@gmail.com](mailto:mint.mercier@gmail.com) ; [sophie.j.evers@gmail.com](mailto:sophie.j.evers@gmail.com)).


#### Notes

[^1]: Rencontre autour d’un thème commun de deux disciplines et donc de ses chercheurs, où chacun garde ses concepts, ses méthodes, sa spécificité.
[^2]: Interaction entre plusieurs spécialités et spécialistes, partage des approches et des concepts afin de résoudre des problèmes globaux et complexes. 
[^3]: l’Institut de la transition environnementale Sorbonne Université : https://www.su-ite.eu/linstitut/quest-ce-que-sui-te/
[^4]: Bicursiosité, *Les activités* : http://bicursiosite.com/activites/ 






